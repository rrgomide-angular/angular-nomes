import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nomes: string[] = [];

  constructor() {
    this.inserirNome('Raphael');
    this.inserirNome('Zulmira');
    this.inserirNome('Vanessa');
    this.inserirNome('Ricardo');
    this.inserirNome('Marcelle');

    this.excluirNome('Raphael');
    console.log(this.nomes);
  }

  inserirNome(nome) {
    this.nomes.push(nome);
    this.nomes.sort();
  }

  excluirNome(nome) {
    this.nomes = this.nomes.filter(item => item !== nome);
  }
}
